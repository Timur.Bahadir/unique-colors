#include <deque>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>

#include <SDL2/SDL.h>

#include <PixelRenderer/SDL_PixelRenderer.hpp>

#include "save_texture.hpp"

#ifdef __WIN32
#include "windows.h"
#endif

constexpr uintmax_t Calc_Colors(uint8_t pc, uint8_t cc) {
  uintmax_t sum{1};
  for (uint8_t i{0}; i < cc; ++i) {
    sum *= pc;
  }
  return sum;
}

constexpr uint8_t Detail_Per_Channel = std::numeric_limits<uint8_t>().max();
constexpr uint8_t Channel_Count = 3;
constexpr uintmax_t Unique_Color_Count =
    Calc_Colors(Detail_Per_Channel, Channel_Count);
constexpr uint16_t Texture_Size = sqrt(Unique_Color_Count);

struct Point {
  uint16_t x, y;
};

#ifdef __WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPSTR lpCmdLine,
                   int cmdShow)
#else
int main(int argc, char **argv)
#endif // __WIN32
{
  std::cout << "Detail per channel: " << Detail_Per_Channel << "\n";
  std::cout << "Channel count: " << Channel_Count << "\n";
  std::cout << "Unique color count: " << Unique_Color_Count << "\n";
  std::cout << "Texture size: " << Texture_Size << std::endl;

  std::deque<Point> possible_positions{};
  possible_positions.push_back(Point{Texture_Size / 2, Texture_Size / 2});

  SDL_Init(SDL_INIT_EVERYTHING);

  std::ostringstream oss;
  oss << "Unique Colors - Unique Color Count: " << Unique_Color_Count
      << ", Generated Colors: 0";
  std::string title = oss.str();

  pr::WindowSettings const ws{title.c_str(), 1400, 1400, Texture_Size,
                              Texture_Size};
  pr::SDL_PixelRenderer renderer{ws};

  bool *used = new bool[Texture_Size * Texture_Size]{false};
  uintmax_t generated_colors{0};
  SDL_Event event;
  for (uint8_t r = 0; r < Detail_Per_Channel; ++r) {
    for (uint8_t g = 0; g < Detail_Per_Channel; ++g) {
      uint32_t current_color{};
      for (uint8_t b = 0; b < Detail_Per_Channel; ++b) {
        if (possible_positions.size() == 0) {
          break;
        }
        std::size_t const size = possible_positions.size();
        std::size_t const random = rand();
        std::size_t const index = random % size;
        Point const next_pos = possible_positions.at(index);

        uint32_t color = ((int)r << 16) | ((int)g << 8) | ((int)b);
        current_color = color;
        ++generated_colors;

        renderer.change_pixel(next_pos.x, next_pos.y, color);

        possible_positions.erase(possible_positions.begin() + index);
        std::size_t const size2 = possible_positions.size();

        used[next_pos.y * Texture_Size + next_pos.x] = true;

        if (next_pos.x > 0 && next_pos.x < Texture_Size - 1 && next_pos.y > 0 &&
            next_pos.y < Texture_Size - 1) {

          for (int8_t y = -1; y < 2; ++y) {
            for (int8_t x = -1; x < 2; ++x) {
              if (y == 0 && x == 0) {
                continue;
              }
              Point const point{next_pos.x - x, next_pos.y - y};

              if (!used[point.y * Texture_Size + point.x]) {
                possible_positions.push_back(point);
                used[point.y * Texture_Size + point.x] = true;
              }
              std::size_t const size3 = possible_positions.size();
            }
          }
        }
      }

      while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
          std::terminate();
          break;
        }
      }

      float percent =
          ((float)generated_colors / (float)Unique_Color_Count) * 100.0f;

      std::ostringstream oss;
      oss << "Unique Colors - Unique Color Count: " << Unique_Color_Count
          << ", Generated Colors: " << generated_colors << ", Current Color: 0x"
          << std::hex << current_color
          << ", Pos Pos: " << possible_positions.size()
          << ", Progress: " << percent;
      std::string title = oss.str();
      SDL_SetWindowTitle(renderer.sdl_window(), title.c_str());
      renderer.draw();
    }
  }

  SDL_SetWindowTitle(renderer.sdl_window(), "Unique Colors - Done!");
  save_texture(renderer.sdl_renderer(), renderer.sdl_texture(), "image.bmp");

  bool running = true;
  while (running) {
    while (SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT) {
        running = false;
        break;
      }
    }

    renderer.draw();
  }

  delete[] used;
  SDL_Quit();
  return 0;
}