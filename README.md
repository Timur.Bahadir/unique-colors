# Unique Colors

A fun little program, that creates an image containing all
possible colors that can be defined using values between
0-255 for the r,g and b channels.

## Building

Requirements:

- Installed CMake version 3.5
- Installed SDL2 library (booth development and runtime)

To build using CMake run:

```bash
mkdir build
cd build
cmake ..
cmake --build .
```

## Output

Below is an example output.

![Demo Video](media/demo.png)
